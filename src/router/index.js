import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
  Auth,
  Landing,
  Login,
  Register,
  Home,
  Agenda,
  AgendaDetail,
  Profile,
  EditProfile,
  Lainnya,
  Event,
  Pengumuman,
  PengumumanDetail,
  Notifikasi,
  Searching,
} from '../pages';
import {BottomNavigator} from '../components/moleculs';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator tabBar={(props) => <BottomNavigator {...props} />}>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Agenda" component={Agenda} />
      <Tab.Screen name="Profile" component={Profile} />
    </Tab.Navigator>
  );
};
const Router = () => {
  return (
    <Stack.Navigator initialRouteName="Auth">
      <Stack.Screen
        name="Auth"
        component={Auth}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Landing"
        component={Landing}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Register"
        component={Register}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="MainApp"
        component={MainApp}
        options={{headerShown: false}}
        path="home"
      />
      <Stack.Screen
        name="Edit Profile"
        component={EditProfile}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Event"
        component={Event}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Agenda"
        component={Agenda}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Agenda Detail"
        component={AgendaDetail}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Pengumuman Detail"
        component={PengumumanDetail}
        options={{headerShown: false}}
        path="details/:eventId"
      />
      <Stack.Screen
        name="Pengumuman"
        component={Pengumuman}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Lainnya"
        component={Lainnya}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Notifikasi"
        component={Notifikasi}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Searching"
        component={Searching}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default Router;
