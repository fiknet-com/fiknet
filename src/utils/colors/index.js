const mainColors = {
  orange: '#FF4500',
  orange1: '#FF5C00',
  red: '#EC1E1E',
  red2: '#E06379',
  grey: '#8C8C8C',
  grey1: '#7E7E7E',
  grey2: '#F6F6F6',
  grey3: '#737373',
  grey4: '#dedede',
  black: '#191E24',
  black1: 'rgba(0,0,0,0.3)',
  white: '#ffff',
};

export const colors = {
  primary: mainColors.orange,
  secondary: mainColors.orange1,
  primaryGrey: mainColors.grey,
  secondaryGrey: mainColors.grey1,
  thirdGrey: mainColors.grey2,
  quartGrey: mainColors.grey4,
  lineGrey: mainColors.grey3,
  primaryWhite: mainColors.white,
  primaryBlack: mainColors.black,
  error: mainColors.red2,
  notifications: mainColors.red,

  text: {
    primary: mainColors.black,
    secondary: mainColors.white,
    title: mainColors.orange,
    subTitle: mainColors.grey1,
    subTitle1: mainColors.grey3,
    active: mainColors.orange,
    inactive: mainColors.grey,
  },
  input: {
    primary: mainColors.grey2,
    secondary: mainColors.white,
  },
  button: {
    primary: {
      background: mainColors.orange,
      text: 'white',
    },
    secondary: {
      background: 'white',
      text: mainColors.orange1,
    },
  },
  loadingBackground: mainColors.black1,
  error: mainColors.red2,
};
