import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import {colors} from '../../../utils';
import {Gap} from '../../atoms';
import {Icon} from 'native-base';

const More = ({title, icon, onPress}) => {
  return (
    <TouchableOpacity style={styles.notifications} onPress={onPress}>
      <View style={styles.headLine}>
        <Image source={icon} style={styles.image} />
        <Text style={styles.title}>{title}</Text>
        <View style={styles.moreOption}>
          <Text style={styles.moreText}>Selengkapnya</Text>
          <Icon style={styles.iconStyle} name="chevron-forward" />
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default More;

const styles = StyleSheet.create({
  notifications: {
    backgroundColor: colors.primaryWhite,
    paddingHorizontal: 16,
    paddingVertical: 20,
  },
  image: {
    height: 30,
  },
  headLine: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    color: colors.text.primary,
    paddingLeft: '3%',
  },
  subtitle: {
    fontSize: 12,
    color: colors.text.subTitle,
  },
  moreOption: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  moreOptionDetail: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'red',
  },
  moreText: {
    fontSize: 10,
    color: colors.text.subTitle,
    alignSelf: 'center',
  },
  iconStyle: {
    fontSize: 17,
    color: colors.text.subTitle,
    alignSelf: 'center',
  },
});
