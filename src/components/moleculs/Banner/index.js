import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {colors, base_img} from '../../../utils';
import {Gap} from '../../atoms';
import moment from 'moment';

const Banner = ({photo, onPress, category, time, title}) => {
  const momentTime = moment(time).fromNow();
  return (
    <TouchableOpacity style={styles.bundleMedia} onPress={onPress}>
      <View style={styles.bundleImg}>
        <Image
          source={{
            uri: `${base_img}/${photo}`,
          }}
          style={styles.mediaImage}
        />
      </View>
      <Gap width={10} />
      <View style={styles.mediaDetail}>
        <Gap height={5} />
        <View style={styles.mediaInfo}>
          <Text style={styles.mediaCategory}>{category}</Text>
          <Text style={styles.mediaTime}>{momentTime}</Text>
        </View>
        <Gap height={5} />
        <Text style={styles.mediaTitle}>{title}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default Banner;

const styles = StyleSheet.create({
  bundleMedia: {
    flexDirection: 'row',
    height: 105,
    borderRadius: 13,
    backgroundColor: colors.primaryWhite,
    elevation: 2,
  },
  bundleImg: {},
  mediaImage: {
    width: 121,
    height: 104,
    resizeMode: 'cover',
    borderTopLeftRadius: 13,
    borderBottomLeftRadius: 13,
  },
  mediaDetail: {
    flex: 1,
  },
  mediaInfo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  mediaCategory: {
    fontSize: 10,
    fontWeight: 'bold',
    color: colors.text.subTitle,
  },
  mediaTime: {
    fontSize: 7,
    color: colors.text.subTitle,
    paddingRight: '5%',
  },
  mediaTitle: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.text.primary,
    flexShrink: 1,
  },
});
