import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {colors} from '../../../utils';
import {Gap} from '../../atoms';

const TimelineDetails = ({title, time, place}) => {
  return (
    <View style={styles.bundle}>
      <Text style={styles.title}>{title}</Text>
      <Gap height={16} />
      <Text style={styles.subTitle}>Waktu : {time}</Text>
      <Gap height={6} />
      <Text style={styles.subTitle}>Tempat : {place}</Text>
    </View>
  );
};

export default TimelineDetails;

const styles = StyleSheet.create({
  bundle: {
    paddingHorizontal: 15,
    paddingVertical: 20,
    backgroundColor: colors.primaryWhite,
  },
  title: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.text.primary,
  },
  subTitle: {
    fontSize: 12,
    color: colors.text.subTitle,
  },
});
