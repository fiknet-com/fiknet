import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {colors} from '../../../utils';
import {Gap} from '../../atoms';

const Comment = ({photos, userName, date, content}) => {
  return (
    <View style={styles.bundleComment}>
      <View style={styles.commentHead}>
        <Image
          source={{
            uri: `${photos}`,
          }}
          style={styles.senderImage}
        />
        <Gap width={12} />
        <Text style={styles.userName}>{userName}</Text>
        <View style={styles.commentDate}>
          <Text style={styles.commentDateText}>{date}</Text>
        </View>
      </View>
      <Text style={styles.commentPost}>{content}</Text>
      <Gap height={8} />
    </View>
  );
};

export default Comment;

const styles = StyleSheet.create({
  bundleComment: {
    height: 90,
    backgroundColor: colors.primaryWhite,
    paddingVertical: 11,
    paddingHorizontal: 11,
    elevation: 3,
    borderRadius: 8,
  },
  commentHead: {
    flex: 1,
    flexDirection: 'row',
  },
  senderImage: {
    width: 28,
    height: 28,
    borderRadius: 14,
  },
  userName: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.text.primary,
    paddingVertical: '1%',
  },
  commentDate: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  commentDateText: {
    fontSize: 11,
    paddingVertical: '3%',
    color: colors.text.subTitle1,
  },
  commentPost: {
    fontSize: 13,
    color: colors.text.primary,
  },
});
