import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {base_img, colors} from '../../../utils';
import {Gap} from '../../atoms';
import moment from 'moment';

const Announcement = ({photo, onPress, time, title}) => {
  const momentTime = moment(time).fromNow();
  return (
    <TouchableOpacity style={styles.bundleMedia} onPress={onPress}>
      <View>
        <Image
          source={{
            uri: `${base_img}/${photo}`,
          }}
          style={styles.mediaImage}
        />
      </View>
      <Gap width={10} />
      <View style={styles.mediaDetail}>
        <Text style={styles.mediaTitle}>{title}</Text>
        <View style={styles.mediaInfo}>
          <Text style={styles.mediaCategory}>{momentTime}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default Announcement;

const styles = StyleSheet.create({
  bundleMedia: {
    flexDirection: 'row',
    height: 105,
    backgroundColor: colors.primaryWhite,
    borderBottomColor: colors.lineGrey,
    borderBottomWidth: StyleSheet.hairlineWidth,
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
  mediaImage: {
    height: 82,
    width: 82,
    borderRadius: 4,
    resizeMode: 'cover',
  },
  mediaDetail: {
    flex: 1,
    marginLeft: '3%',
  },
  mediaInfo: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
  mediaCategory: {
    fontSize: 12,
    color: colors.text.subTitle,
  },
  mediaTime: {
    fontSize: 7,
    color: colors.text.subTitle,
    paddingRight: '5%',
  },
  mediaTitle: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.text.primary,
    flexShrink: 1,
  },
});
