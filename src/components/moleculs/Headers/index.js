import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {Icon} from 'native-base';
import {colors} from '../../../utils';

const Headers = ({title, type, iconName, onPressBack, onPressRight}) => {
  const Heading = () => {
    if (type === 'solo') {
      return (
        <>
          <View>
            <Text style={styles.title}>{title}</Text>
          </View>
        </>
      );
    } else if (type === 'double') {
      return (
        <>
          <View>
            <Text style={styles.title}>{title}</Text>
          </View>
          <TouchableOpacity onPress={onPressRight}>
            <Icon style={styles.iconStyle} name={iconName} />
          </TouchableOpacity>
        </>
      );
    } else if (type === 'triple') {
      return (
        <>
          <View style={styles.leftSide}>
            <TouchableOpacity onPress={onPressBack}>
              <Icon style={styles.iconStyle} name="chevron-back-outline" />
            </TouchableOpacity>
            <View style={styles.bundleTitle}>
              <Text style={styles.title}>{title}</Text>
            </View>
          </View>
          <TouchableOpacity onPress={onPressRight}>
            <Icon style={styles.iconStyle} name={iconName} />
          </TouchableOpacity>
        </>
      );
    } else if (type === 'edit') {
      return (
        <>
          <View style={styles.leftSide}>
            <Text style={styles.title}>{title}</Text>
          </View>
          <TouchableOpacity onPress={onPressRight}>
            <Icon style={styles.iconStyle} name={iconName} />
          </TouchableOpacity>
        </>
      );
    }
  };
  return (
    <View style={styles.header}>
      <Heading />
    </View>
  );
};

export default Headers;

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    backgroundColor: colors.primary,
    justifyContent: 'space-between',
    paddingVertical: 13,
    paddingHorizontal: 12,
    paddingTop: '10%',
  },
  bundleTitle: {
    marginLeft: 13,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.text.secondary,
  },
  iconStyle: {
    fontSize: 26,
    alignSelf: 'center',
    color: colors.text.secondary,
  },
  leftSide: {
    flexDirection: 'row',
  },
});
