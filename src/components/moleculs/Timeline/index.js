import {Icon} from 'native-base';
import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {colors} from '../../../utils';

const Timeline = ({dates, title, subtitle, onPress}) => {
  const string = dates;
  const newDate = string.split(' ');
  console.log('isi string ', newDate);
  return (
    <TouchableOpacity onPress={onPress} style={styles.timeline}>
      <View style={styles.date}>
        <Text style={styles.dateDetail}>
          {newDate[0]}
          {'\n'}
          {newDate[1]}
          {'\n'}
          {newDate[2]}
          {'\n'}
        </Text>
      </View>
      <View style={styles.bundeInfo}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.subTitle}>{subtitle}</Text>
        <View style={styles.moreOption}>
          <View>
            <Text style={styles.moreText}>Selengkapnya</Text>
          </View>
          <View>
            <Icon style={styles.iconStyle} name="chevron-forward" />
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default Timeline;

const styles = StyleSheet.create({
  timeline: {
    height: 110,
    flexDirection: 'row',
    paddingVertical: 18,
    paddingHorizontal: 15,
    borderBottomColor: colors.lineGrey,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  date: {
    width: 70,
    height: 70,
    borderRadius: 8,
    backgroundColor: colors.quartGrey,
    paddingHorizontal: 13,
    paddingVertical: 8,
  },
  dateDetail: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.text.title,
    flexShrink: 1,
    textAlign: 'center',
    zIndex: 1,
  },
  title: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.text.primary,
  },
  subTitle: {
    fontSize: 12,
    color: colors.text.primary,
  },
  moreOption: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  moreOptionDetail: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'red',
  },
  moreText: {
    fontSize: 10,
    color: colors.text.subTitle,
    alignSelf: 'center',
  },
  iconStyle: {
    fontSize: 17,
    color: colors.text.subTitle,
    alignSelf: 'center',
  },
  bundeInfo: {
    flex: 1,
    marginLeft: '5%',
  },
});
