import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {colors} from '../../../utils';

const Category = ({photos, title, onPress}) => {
  return (
    <View style={styles.category}>
      <TouchableOpacity style={styles.bundleCategory} onPress={onPress}>
        <Image source={photos} style={styles.imagesCategory} />
      </TouchableOpacity>
      <Text style={styles.text}>{title}</Text>
    </View>
  );
};

export default Category;

const styles = StyleSheet.create({
  category: {
    width: 75,
  },
  bundleCategory: {
    width: 70,
    height: 70,
    borderRadius: 8,
    backgroundColor: colors.primaryWhite,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 2,
  },
  imagesCategory: {
    width: 40,
    height: 40,
    alignItems: 'center',
  },
  text: {
    textAlign: 'center',
    paddingTop: 8,
    fontSize: 12,
    color: colors.text.subTitle,
  },
});
