import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {colors} from '../../../utils';
import {Gap} from '../../atoms';

const Notif = ({title, time, contain, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.notifications}>
        <View style={styles.headLine}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.subtitle}>{time}</Text>
        </View>
        <Gap height={7} />
        <Text style={styles.subtitle}>{contain}</Text>
      </View>
      <Gap height={10} />
    </TouchableOpacity>
  );
};

export default Notif;

const styles = StyleSheet.create({
  notifications: {
    backgroundColor: colors.primaryWhite,
    paddingHorizontal: 16,
    paddingVertical: 20,
  },
  headLine: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    color: colors.text.primary,
  },
  subtitle: {
    fontSize: 12,
    color: colors.text.subTitle,
  },
});
