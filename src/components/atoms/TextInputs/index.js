import React from 'react';
import {StyleSheet, TextInput, Text, View} from 'react-native';
import {colors} from '../../../utils';

const TextInputs = ({
  title,
  value,
  edit,
  placeholder,
  onChangeText,
  secure,
}) => {
  return (
    <View style={styles.input}>
      <Text style={styles.titleText}>{title}</Text>
      <TextInput
        value={value}
        style={styles.inputText}
        secureTextEntry={secure}
        editable={edit}
        onChangeText={onChangeText}
        placeholder={placeholder}
      />
    </View>
  );
};

export default TextInputs;

const styles = StyleSheet.create({
  textInput: {
    backgroundColor: colors.input.primary,
    color: colors.text.subTitle,
    fontSize: 18,
    paddingHorizontal: 16,
    paddingVertical: 15,
    opacity: 0.8,
    height: 48,
    borderRadius: 6,
  },
  input: {
    backgroundColor: colors.input.secondary,
    borderBottomColor: colors.primary,
    borderBottomWidth: 2,
    paddingHorizontal: 12,
  },
  titleText: {
    fontSize: 12,
    color: colors.text.title,
  },
  inputText: {
    fontSize: 16,
    color: colors.text.primary,
    backgroundColor: colors.input.secondary,
  },
});
