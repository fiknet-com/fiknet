import React from 'react';
import {StatusBar} from 'react-native';
import {colors} from '../../../utils';

const StatusBars = () => {
  return <StatusBar backgroundColor="transparent" translucent={true} />;
};

export default StatusBars;
