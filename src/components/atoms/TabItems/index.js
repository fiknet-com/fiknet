import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {colors} from '../../../utils';

const TabItems = ({title, active, onLongPress, onPress}) => {
  const url = '../../../assets';
  const IconMenu = () => {
    if (title === 'Home') {
      return (
        <Image
          source={
            active
              ? require('../../../assets/home-active.png')
              : require('../../../assets/home-passive.png')
          }
          style={styles.image}
        />
      );
    }
    if (title === 'Agenda') {
      return (
        <Image
          source={
            active
              ? require('../../../assets/agenda-active.png')
              : require('../../../assets/agenda-passive.png')
          }
          style={styles.image}
        />
      );
    }
    if (title === 'Profile') {
      return (
        <Image
          source={
            active
              ? require('../../../assets/profile-active.png')
              : require('../../../assets/profile-passive.png')
          }
          style={styles.image}
        />
      );
    }
    return (
      <Image
        source={
          active
            ? require('../../../assets/home-active.png')
            : require('../../../assets/home-passive.png')
        }
        style={styles.image}
      />
    );
  };
  return (
    <TouchableOpacity
      onPress={onPress}
      onLongPress={onLongPress}
      style={styles.container}>
      <IconMenu />
      <Text style={styles.text(active)}>{title}</Text>
    </TouchableOpacity>
  );
};

export default TabItems;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  iconStyle: (active) => ({
    fontSize: 25,
    color: active ? colors.text.primary : colors.text.inactive,
  }),
  text: (active) => ({
    color: active ? colors.text.active : colors.text.inactive,
    fontSize: 12,
    fontWeight: 'bold',
  }),
});
