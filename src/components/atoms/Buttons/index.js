import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {colors} from '../../../utils';

const Buttons = ({onPress, title, isPrimary, light}) => {
  return (
    <TouchableOpacity style={styles.button(isPrimary, light)} onPress={onPress}>
      <Text style={styles.textButton(light)}>{title}</Text>
    </TouchableOpacity>
  );
};

export default Buttons;

const styles = StyleSheet.create({
  button: (isPrimary, light) => ({
    backgroundColor: light ? colors.primaryWhite : colors.primary,
    paddingVertical: '3%',
    height: 48,
    borderRadius: 24,
    marginHorizontal: isPrimary ? 15 : 74,
  }),
  textButton: (light) => ({
    color: light ? colors.text.title : colors.text.secondary,
    fontSize: 16,
    textAlign: 'center',
    paddingHorizontal: 40,
  }),
});
