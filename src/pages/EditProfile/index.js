import axios from 'axios';
import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native';
import {base_url, colors, getData} from '../../utils';
import {Announcement, Headers, Timeline} from '../../components/moleculs';
import {Buttons, Gap, StatusBars, TextInputs} from '../../components/atoms';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {showMessage} from 'react-native-flash-message';

const EditProfile = ({navigation, route}) => {
  const data = {
    ...route.params,
    password: '',
  };
  const [dataProfile, setDataProfile] = useState(data);
  const [photo, setPhoto] = useState(
    dataProfile.image
      ? dataProfile.image
      : 'https://i.ibb.co/L82vc7B/profile-base.png',
  );
  const [loading, setLoading] = useState(false);

  const changeText = (key, value) => {
    setDataProfile({
      ...dataProfile,
      [key]: value,
    });
  };

  const getImage = () => {
    launchImageLibrary(
      {quality: 0.3, minHeight: 110, minWidth: 110, includeBase64: true},
      (response) => {
        if (response.didCancel || response.errorMessage) {
          showMessage({
            message: 'oops, sepertinya anda tidak jadi upload foto ?',
            type: 'default',
            backgroundColor: colors.error,
            color: colors.title,
          });
        } else {
          const source = `data:${response.type};base64, ${response.base64}`;
          const sourceLocal = {uri: response.uri};
          // setPhoto(source);
          setPhoto(source);
          console.log('data sourceLokal : ', sourceLocal);
          console.log('data source : ', source);
        }
      },
    );
  };

  const onSave = () => {
    if (
      !dataProfile.password.trim() ||
      !dataProfile.name.trim() ||
      !dataProfile.nim.trim() ||
      !dataProfile.email.trim()
    ) {
      showMessage({
        message: 'dont let blank input',
        type: 'default',
        backgroundColor: colors.notifications,
        color: colors.text.secondary,
      });
      return;
    } else {
      console.log('isi data : ', dataProfile);
      setLoading(true);
      getData('login_log').then((res) => {
        if (res) {
          const data = {
            name: dataProfile.name,
            email: dataProfile.email,
            password: dataProfile.password,
            image: photo,
            nim: dataProfile.nim,
          };
          console.log('data : ', data);
          updateProfile(res.token, res.id, data);
        }
      });
    }
    const updateProfile = (token, id, dataUser) => {
      axios
        .put(`${base_url}/auth/user/${id}`, dataUser, {
          headers: {
            Authorization: `bearer ${token}`,
          },
        })
        .then((response) => {
          setLoading(false);
          showMessage({
            message: 'Data telah diubah !',
            type: 'success',
            color: colors.text.secondary,
          });
          navigation.replace('Login');
          console.log('data berhasil diupdate ke db', response);
        })
        .catch((error) => {
          setLoading(false);
          console.log('error : ', error.response.data);
          showMessage({
            message: 'Oops!, sepertinya file yang anda pilih terlalu besar',
            type: 'default',
            backgroundColor: colors.error,
            color: colors.text.secondary,
          });
        });
    };
  };
  return (
    <ScrollView style={styles.container}>
      <StatusBars />
      <Headers type="solo" title="Edit Profil" />
      <View style={styles.topSection}>
        <View style={styles.bundleImage}>
          <Image source={{uri: photo}} style={styles.image} />
          <TouchableOpacity onPress={getImage} style={styles.bundleImageEdit}>
            <Gap height={'60%'} />
            <Image
              source={require('../../assets/profile_photo.png')}
              style={styles.imageEdit}
            />
          </TouchableOpacity>
        </View>
        <Image
          source={require('../../assets/profile_bg.png')}
          style={styles.imageBg}
        />
      </View>
      <View style={styles.bundle}>
        <Gap height={30} />
        <TextInputs
          light
          title="Nama"
          value={dataProfile.name}
          onChangeText={(value) => changeText('name', value)}
        />
        <Gap height={20} />
        <TextInputs
          light
          value={dataProfile.nim}
          title="NIM"
          onChangeText={(value) => changeText('nim', value)}
        />
        <Gap height={20} />
        <TextInputs
          light
          value={dataProfile.email}
          title="Email"
          onChangeText={(value) => changeText('email', value)}
        />
        <Gap height={20} />
        <TextInputs
          light
          placeholder="Masukkan Password"
          title="Kata Sandi"
          onChangeText={(value) => changeText('password', value)}
          secure
        />
        <Gap height={40} />
        <Buttons title="Simpan" onPress={onSave} />
      </View>
    </ScrollView>
  );
};

export default EditProfile;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
  },
  topSection: {
    height: 122,
    borderBottomLeftRadius: 25,
    borderBottomRightRadius: 25,
    backgroundColor: colors.primary,
  },
  image: {
    width: 110,
    height: 110,
    borderRadius: 55,
    resizeMode: 'cover',
    alignSelf: 'center',
    zIndex: 1,
  },
  bundleImage: {
    justifyContent: 'center',
    zIndex: 1,
  },
  bundleImageEdit: {
    zIndex: 1,
    position: 'absolute',
    alignSelf: 'center',
  },
  imageEdit: {
    width: 40,
    height: 40,
    alignSelf: 'flex-end',
    marginLeft: '25%',
  },
  imageBg: {
    position: 'absolute',
    zIndex: 0,
    alignSelf: 'center',
    width: '100%',
    resizeMode: 'stretch',
  },
  bundle: {
    marginHorizontal: 13,
    marginTop: '10%',
  },
});
