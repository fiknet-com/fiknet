import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {StyleSheet, Text, View, ScrollView} from 'react-native';
import {base_url, colors, getData} from '../../utils';
import {Announcement, Headers, Timeline} from '../../components/moleculs';
import {StatusBars} from '../../components/atoms';

const Pengumuman = ({navigation, route}) => {
  const data = route.params;
  const item = data.title;

  const [event, setEvent] = useState([]);

  useEffect(() => {
    renderData();
  }, []);

  const renderData = () => {
    getData('login_log').then((res) => {
      if (res) {
        getEvent(res.token);
      }
    });
  };
  const getEvent = (token) => {
    axios
      .get(`${base_url}/event/posts/${item}`, {
        headers: {
          Authorization: `bearer ${token}`,
        },
      })
      .then((response) => {
        setEvent(response.data.data);
        console.log('data berhasil diretrive ', response.data.data);
      })
      .catch((error) => {
        console.log('error : ', error.message);
      });
  };
  return (
    <ScrollView style={styles.container}>
      <StatusBars />
      <View>
        <Headers
          type="triple"
          title={item}
          iconName="search"
          onPressBack={() => navigation.goBack()}
          onPressRight={() =>
            navigation.navigate('Searching', {url: `event/posts/${item}`})
          }
        />
        {event.map((item) => {
          return (
            <Announcement
              key={item._id}
              title={item.tittle}
              photo={item.image}
              time={item.createdAt}
              onPress={() => navigation.navigate('Pengumuman Detail', item)}
            />
          );
        })}
      </View>
    </ScrollView>
  );
};

export default Pengumuman;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.primaryWhite,
    flex: 1,
  },
});
