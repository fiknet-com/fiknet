import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Gap, StatusBars} from '../../components/atoms';
import {Headers, Notif} from '../../components/moleculs';
import {colors} from '../../utils';

const Notifikasi = () => {
  return (
    <View>
      <StatusBars />
      <Headers title="Notifikasi" type="triple" iconName="search" />

      <Notif
        title="Pengumuman"
        time="09.40"
        contain="
          Pengisian Evaluasi Dosen oleh Mahasiswa dan Batas Input Nilai UAS"
      />
      <Notif
        title="Pengumuman"
        time="09.40"
        contain="
          Pengisian Evaluasi Dosen oleh Mahasiswa dan Batas Input Nilai UAS"
      />
      <Notif
        title="Pengumuman"
        time="09.40"
        contain="
          Pengisian Evaluasi Dosen oleh Mahasiswa dan Batas Input Nilai UAS"
      />
    </View>
  );
};

export default Notifikasi;

const styles = StyleSheet.create({});
