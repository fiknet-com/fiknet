import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {getData} from '../../utils';

const Auth = ({navigation}) => {
  useEffect(() => {
    const _validasiSession = async () => {
      const isLogin = await getData('login_log');
      navigation.replace(isLogin ? 'MainApp' : 'Landing');
    };
    _validasiSession();
  }, [navigation]);
  return null;
};

export default Auth;

const styles = StyleSheet.create({});
