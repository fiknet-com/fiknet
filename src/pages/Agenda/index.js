import axios from 'axios';
import {Icon} from 'native-base';
import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View, ScrollView} from 'react-native';
import {base_url, colors, getData} from '../../utils';
import {Headers, Timeline} from '../../components/moleculs';

const Agenda = ({navigation}) => {
  const [event, setEvent] = useState([]);
  useEffect(() => {
    renderData();
    const unsubscribe = navigation.addListener('focus', () => {
      renderData();
    });
    return unsubscribe;
  }, [navigation]);

  const renderData = () => {
    getData('login_log').then((res) => {
      if (res) {
        getEvent(res.token);
      }
    });
  };
  const getEvent = (token) => {
    axios
      .get(`${base_url}/agenda/posts`, {
        headers: {
          Authorization: `bearer ${token}`,
        },
      })
      .then((response) => {
        setEvent(response.data.data);
        console.log('data berhasil diretrive ', response.data.data);
      })
      .catch((error) => {
        console.log('error : ', error.message);
      });
  };
  return (
    <ScrollView style={styles.container}>
      <Headers
        type="double"
        title="Agenda"
        iconName="search"
        onPressRight={() =>
          navigation.navigate('Searching', {
            url: `agenda/posts`,
            category: 'agenda',
          })
        }
      />
      {event.reverse().map((item) => {
        return (
          <Timeline
            key={item._id}
            dates={item.date}
            title={item.tittle}
            subtitle={item.place}
            onPress={() => navigation.navigate('Agenda Detail', item)}
          />
        );
      })}
    </ScrollView>
  );
};

export default Agenda;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primaryWhite,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.text.secondary,
  },
  iconStyle: {
    fontSize: 20,
    color: colors.text.secondary,
  },
});
