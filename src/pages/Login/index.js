import axios from 'axios';
import React, {useState} from 'react';
import {StyleSheet, Text, View, Image, TextInput} from 'react-native';
import {Buttons, Gap, Loading, StatusBars} from '../../components/atoms';
import TextInputs from '../../components/atoms/TextInputs';
import {showMessage} from 'react-native-flash-message';
import {base_url, colors, storeData, useForm} from '../../utils';

const Login = ({navigation}) => {
  const [loading, setLoading] = useState(false);
  const [form, setForm] = useForm({
    email: '',
    password: '',
  });
  const onContinue = () => {
    console.log('isi form : ', form);
    setLoading(true);
    axios
      .post(`${base_url}/auth/login`, form)
      .then((response) => {
        setLoading(false);
        showMessage({
          message: 'Login Success!',
          type: 'success',
          color: colors.text.secondary,
        });
        setForm('reset');
        const data = {
          email: form.email,
          id: response.data.id,
          token: response.data.token,
        };
        storeData('login_log', data);
        navigation.replace('MainApp');
        console.log('data berhasil dikirim ke db', data);
      })
      .catch((error) => {
        setLoading(false);
        console.log('errr : ', error.message);
        showMessage({
          message: 'Username dan Password tidak ditemukan',
          type: 'default',
          backgroundColor: colors.notifications,
          color: colors.text.secondary,
        });
      });
  };
  return (
    <>
      <View style={styles.container}>
        <StatusBars />
        <Image
          source={require('../../assets/login_bg.png')}
          style={styles.gradienTop}
        />
        <View style={styles.bundle}>
          <Image
            source={require('../../assets/logo-nodesc.png')}
            style={styles.logo}
          />
          <Gap height={63} />
          <TextInput
            placeholder="Email"
            value={form.email}
            onChangeText={(value) => {
              setForm('email', value);
            }}
            style={styles.textInput}
          />
          <Gap height={9} />
          <TextInput
            placeholder="Password"
            value={form.password}
            onChangeText={(value) => {
              setForm('password', value);
            }}
            style={styles.textInput}
            secureTextEntry
          />

          <Gap height={25} />
          <Buttons light onPress={onContinue} title="Masuk" />
          <Gap height={16} />

          <Text style={styles.bantuan}>Lupa kata sandi ? Bantuan</Text>
          {/* 
        <View style={styles.register}> */}
          <Gap height={63} />
          <View style={styles.separator}>
            <View style={styles.separatorLine} />
            <Text style={styles.separatorText}>atau</Text>
            <View style={styles.separatorLine} />
          </View>
          <Gap height={20} />

          <Buttons
            title="Belum punya akun? Daftar"
            isPrimary
            onPress={() => navigation.navigate('Register')}
          />
        </View>
      </View>
      {loading && <Loading />}
    </>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  bundle: {
    marginHorizontal: 15,
    flex: 1,
  },
  gradienTop: {
    minWidth: 450,
    minHeight: 130,
    resizeMode: 'cover',
  },
  logo: {
    resizeMode: 'contain',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  bantuan: {
    textAlign: 'center',
    fontSize: 12,
    color: colors.text.secondary,
  },
  separator: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 25,
    alignItems: 'center',
  },
  separatorText: {
    fontSize: 12,
    color: colors.text.secondary,
  },
  separatorLine: {
    backgroundColor: colors.primaryWhite,
    width: '40%',
    height: 2,
  },
  register: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 30,
  },

  textInput: {
    backgroundColor: colors.input.primary,
    color: colors.text.subTitle,
    fontSize: 18,
    paddingHorizontal: 16,
    opacity: 0.8,
    height: 48,
    borderRadius: 6,
  },
});
