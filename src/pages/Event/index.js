import React from 'react';
import {StyleSheet, Text, View, ScrollView} from 'react-native';
import {colors} from '../../utils';
import {Announcement, Headers, Timeline} from '../../components/moleculs';
const Event = () => {
  return (
    <ScrollView style={styles.container}>
      <View>
        <Headers type="triple" title="Profile" iconName="search" />
        <Announcement
          title="Pengisian Evaluasi Dosen oleh Mahasiswa dan Batas Input Nilai UAS"
          time="17 menit yang lalu"
        />
      </View>
    </ScrollView>
  );
};

export default Event;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.primaryWhite,
    flex: 1,
  },
});
