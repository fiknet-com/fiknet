import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {Gap} from '../../components/atoms';
import {Headers, Announcement, Timeline} from '../../components/moleculs';
import {base_url, colors, getData} from '../../utils';
import {Icon} from 'native-base';

const Searching = ({navigation, route}) => {
  const url = route.params.url;
  const category = route.params.category;
  console.log('isi url : ', url);
  const [input, setInput] = useState('');
  const [event, setEvent] = useState([]);
  const [holder, setHolder] = useState([]);

  useEffect(() => {
    renderData();
  }, []);

  const renderData = () => {
    getData('login_log').then((res) => {
      if (res) {
        getInfo(res.token);
      }
    });
  };

  const getInfo = (token) => {
    axios
      .get(`${base_url}/${url}`, {
        headers: {
          Authorization: `bearer ${token}`,
        },
      })
      .then((response) => {
        const data = response.data.data;
        setEvent(data);
        setHolder(data);
        console.log('data parsed : ', data);
      })
      .catch((error) => {
        console.log('error : ', error.message);
      });
  };
  const handleChange = (item) => {
    setInput(item);

    if (input.length > 0) {
      const newData = holder.filter((items) => {
        const itemData = `${items.tittle.toUpperCase()}`;
        const textData = item.toUpperCase();

        return itemData.indexOf(textData) > -1;
      });
      setEvent(newData);
    }
  };
  return (
    <View style={styles.bg}>
      <View>
        <Headers
          type="triple"
          title="Search"
          onPressBack={() => navigation.goBack()}
        />

        <Gap height={15} />

        <View style={styles.container}>
          <View style={styles.bundle}>
            <View style={styles.iconBundle}>
              <Icon
                type="MaterialIcons"
                name="search"
                style={styles.InfoIcon}
              />
            </View>
            <TextInput
              placeholder="Cari disini ..."
              value={input}
              onChangeText={(value) => handleChange(value)}
            />
          </View>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View>
              <Gap height={20} />
            </View>

            {event.reverse().map((item) => {
              if (category === 'agenda') {
                return (
                  <Timeline
                    key={item._id}
                    dates={item.date}
                    title={item.tittle}
                    subtitle={item.place}
                    onPress={() => navigation.navigate('Agenda Detail', item)}
                  />
                );
              }
              return (
                <>
                  <Announcement
                    key={item._id}
                    title={item.tittle}
                    photo={item.image}
                    time={item.createdAt}
                    onPress={() =>
                      navigation.navigate('Pengumuman Detail', item)
                    }
                  />
                  <Gap height={20} />
                </>
              );
            })}
          </ScrollView>
        </View>
      </View>
    </View>
  );
};

export default Searching;

const styles = StyleSheet.create({
  bg: {
    backgroundColor: colors.primaryWhite,
    flex: 1,
  },
  container: {
    marginHorizontal: 16,
  },
  bundle: {
    height: 40,
    backgroundColor: colors.primaryGrey,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 20,
    opacity: 0.5,
  },
  iconBundle: {
    marginLeft: 16,
    marginRight: 12,
    color: colors.primaryGrey,
  },
  textBundle: {
    color: colors.text.subTitle,
    fontSize: 14,
    flexWrap: 'wrap',
  },
  title: {
    color: colors.text.primary,
    fontSize: 20,
    textAlign: 'center',
  },
});
