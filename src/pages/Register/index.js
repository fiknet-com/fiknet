import axios from 'axios';
import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {showMessage} from 'react-native-flash-message';
import {Buttons, Gap, StatusBars, Loading} from '../../components/atoms';
import {colors, storeData, useForm, base_url} from '../../utils';

const Register = ({navigation}) => {
  const [loading, setLoading] = useState(false);
  const [form, setForm] = useForm({
    name: '',
    email: '',
    nim: '',
    password: '',
  });

  const onContinue = () => {
    console.log('isi form : ', form);
    setLoading(true);
    axios
      .post(`${base_url}/auth/user`, form)
      .then((response) => {
        setLoading(false);
        showMessage({
          message: 'Silahkan login',
          type: 'success',
          color: colors.text.secondary,
        });
        setForm('reset');
        const data = {
          ...form,
          token: response.data.token,
        };
        storeData('login_log', data);
        navigation.navigate('Login');
        console.log('data berhasil dikirim ke db', data);
      })
      .catch((error) => {
        setLoading(false);
        showMessage({
          message: error.message,
          type: 'default',
          backgroundColor: colors.notifications,
          color: colors.text.secondary,
        });
      });
  };
  return (
    <>
      <View style={styles.container}>
        <StatusBars />
        <Image
          source={require('../../assets/login_bg.png')}
          style={styles.gradienTop}
        />
        <View style={styles.bundle}>
          <Image
            source={require('../../assets/logo-nodesc.png')}
            style={styles.logo}
          />
          <Gap height={63} />
          <ScrollView showsVerticalScrollIndicator={false}>
            <TextInput
              placeholder="Nama Lengkap"
              value={form.name}
              onChangeText={(value) => {
                setForm('name', value);
              }}
              style={styles.textInput}
            />
            <Gap height={9} />
            <TextInput
              placeholder="NIM"
              value={form.nim}
              onChangeText={(value) => {
                setForm('nim', value);
              }}
              style={styles.textInput}
            />
            <Gap height={9} />
            <TextInput
              placeholder="Email"
              value={form.email}
              onChangeText={(value) => {
                setForm('email', value);
              }}
              style={styles.textInput}
            />
            <Gap height={9} />
            <TextInput
              placeholder="Kata Sandi"
              value={form.password}
              onChangeText={(value) => {
                setForm('password', value);
              }}
              style={styles.textInput}
              secureTextEntry
            />
            <Gap height={45} />
            <Buttons title="Daftar" light onPress={onContinue} />
            <Gap height={26} />
            <TouchableOpacity onPress={() => navigation.navigate('Login')}>
              <Text style={styles.bantuan}>Sudah punya akun? Masuk</Text>
            </TouchableOpacity>
          </ScrollView>
        </View>
      </View>
      {loading && <Loading />}
    </>
  );
};

export default Register;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  bundle: {
    marginHorizontal: 15,
    flex: 1,
  },
  gradienTop: {
    minWidth: 450,
    minHeight: 130,
    resizeMode: 'cover',
  },
  logo: {
    resizeMode: 'contain',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  bantuan: {
    textAlign: 'center',
    fontSize: 12,
    color: colors.text.secondary,
  },
  textInput: {
    backgroundColor: colors.input.primary,
    color: colors.text.subTitle,
    fontSize: 18,
    paddingHorizontal: 16,
    opacity: 0.8,
    height: 48,
    borderRadius: 6,
  },
});
