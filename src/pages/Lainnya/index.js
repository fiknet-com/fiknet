import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Gap, StatusBars} from '../../components/atoms';
import {Headers, More} from '../../components/moleculs';
import {colors} from '../../utils';

const Lainnya = ({navigation}) => {
  return (
    <View>
      <StatusBars />
      <Headers
        title="Lainnya"
        type="triple"
        iconName="search"
        onPressBack={() => navigation.goBack()}
      />
      <More
        title="Pengumuman"
        icon={require('../../assets/prestasi.png')}
        onPress={() => navigation.navigate('Pengumuman', {title: 'Pengumuman'})}
      />
      <Gap height={10} />
      <More
        title="Kerja Sama"
        icon={require('../../assets/teamWork.png')}
        onPress={() => navigation.navigate('Pengumuman', {title: 'Kerja Sama'})}
      />
      <Gap height={10} />
      <More
        title="Informasi Umum"
        onPress={() =>
          navigation.navigate('Pengumuman', {title: 'Informasi Umum'})
        }
        icon={require('../../assets/information.png')}
      />
      <Gap height={10} />
      <More
        title="Profil Dekan"
        icon={require('../../assets/dekan.png')}
        onPress={() =>
          navigation.navigate('Pengumuman', {title: 'Profil Dekan'})
        }
      />
      <Gap height={10} />
    </View>
  );
};

export default Lainnya;

const styles = StyleSheet.create({});
