import React, {useEffect} from 'react';
import {StyleSheet, Text, Image, View} from 'react-native';
import {StatusBars} from '../../components/atoms';
import {colors} from '../../utils';

const Landing = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Login');
    }, 3000);
  }, [navigation]);
  return (
    <View style={styles.container}>
      <StatusBars />
      <View style={styles.bundleimage}>
        <Image source={require('../../assets/logo.png')} style={styles.image} />
      </View>
      <View style={styles.gradienBg}>
        <Image
          source={require('../../assets/landing_bg.png')}
          style={styles.imageBg}
        />
      </View>
    </View>
  );
};

export default Landing;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  bundleimage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
    marginTop: '40%',
  },
  image: {
    resizeMode: 'contain',
  },
  gradienBg: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  imageBg: {
    minWidth: 450,
    minHeight: 300,
    resizeMode: 'cover',
  },
});
