import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {StatusBars, Gap} from '../../components/atoms';
import {Headers, TimelineDetails} from '../../components/moleculs';
const AgendaDetail = ({route, navigation}) => {
  const data = route.params;

  return (
    <View>
      <StatusBars />
      <Headers
        type="triple"
        title="Agenda"
        onPressBack={() => navigation.goBack()}
      />
      <TimelineDetails
        title={data.tittle}
        time={data.date}
        place={data.place}
      />
    </View>
  );
};

export default AgendaDetail;

const styles = StyleSheet.create({});
