import Auth from './Auth';
import Landing from './Landing';
import Login from './Login';
import Register from './Register';
import Home from './Home';
import Agenda from './Agenda';
import AgendaDetail from './AgendaDetail';
import Profile from './Profile';
import EditProfile from './EditProfile';
import Lainnya from './Lainnya';
import Event from './Event';
import Pengumuman from './Pengumuman';
import PengumumanDetail from './PengumumanDetail';
import Notifikasi from './Notifikasi';
import Searching from './Searching';

export {
  Auth,
  Landing,
  Login,
  Register,
  Home,
  Agenda,
  AgendaDetail,
  Profile,
  EditProfile,
  Lainnya,
  Event,
  Pengumuman,
  PengumumanDetail,
  Notifikasi,
  Searching,
};
