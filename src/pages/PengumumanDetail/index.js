import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  Share,
} from 'react-native';
import {
  base_img,
  base_url,
  colors,
  getData,
  getTime,
  useChat,
} from '../../utils';
import {Headers, Comment} from '../../components/moleculs';
import {Icon} from 'native-base';
import {Gap} from '../../components/atoms';
import axios from 'axios';
import moment from 'moment';

const PengumumanDetail = ({navigation, route}) => {
  const data = route.params;
  const [input, setInput] = useState('');
  const [chatID, setChatID] = useState(data._id);
  const [local, setLocal] = useState({});
  const [profile, setProfile] = useState({});
  const {messages, sendMessage, setMessages} = useChat(data._id);

  useEffect(() => {
    renderData();
  }, []);

  const renderData = () => {
    getData('login_log').then(async (res) => {
      if (res) {
        await setLocal(res);
        getChatData(res.token);
        getProfile(res.token, res.id);
      }
    });
  };

  const getChatData = (token) => {
    axios
      .get(`${base_url}/chat/posts/${chatID}`, {
        headers: {
          Authorization: `bearer ${token}`,
        },
      })
      .then(async (response) => {
        if (response) {
          const oldData = response.data.data;
          const data = [];
          console.log('isi olddata :  ', oldData);
          const promises = await Object.keys(oldData).map(async (key) => {
            await axios
              .get(
                `${base_url}/auth/user/${oldData[key].allChat.chatText.sendBy}`,
                {
                  headers: {
                    Authorization: `bearer ${token}`,
                  },
                },
              )
              .then((response) => {
                data.push({
                  id: key,
                  name: response.data.data.name,
                  image: response.data.data.image,
                  ...oldData[key],
                });
              })
              .catch((error) => {
                console.log('error get profile : ', error.message);
              });
          });
          await Promise.all(promises);
          // setListChatPair(data);
          console.log('isi listChatPair: ', data);
          setMessages(data);
          console.log('data baru setelah di parse : ', data);
        }
        console.log('list chat : ', response.data.data);
      })
      .catch((error) => {
        console.log('error disini : ', error.message);
      });
  };

  const getProfile = (token, idUser) => {
    axios
      .get(`${base_url}/auth/user/${idUser}`, {
        headers: {
          Authorization: `bearer ${token}`,
        },
      })
      .then((response) => {
        console.log('respon get user profile : ', response.data.data);
        setProfile(response.data.data);
      })
      .catch((error) => {
        console.log('error get message: ', error);
      });
  };
  const handleSubmit = () => {
    const today = new Date();
    const data = {
      chatID: chatID,
      allChat: {
        dateChat: today.getTime(),
        chatText: {
          sendBy: profile._id,
          userImage: profile.image,
          chatTime: getTime(today),
          chatContent: input,
        },
      },
    };
    axios
      .post(`${base_url}/chat/post`, data, {
        headers: {
          Authorization: `bearer ${local.token}`,
        },
      })
      .then((response) => {
        setInput('');
        console.log('data berhasil dikirim ke db', data);
      })
      .catch((error) => {
        showMessage({
          message: error.message,
          type: 'default',
          backgroundColor: colors.error,
          color: colors.text.title,
        });
      });
    const dataLocal = {
      name: profile.name,
      image: profile.image,
      chatID: chatID,
      allChat: {
        dateChat: today.getTime(),
        chatText: {
          sendBy: profile._id,
          userImage: profile.image,
          chatTime: getTime(today),
          chatContent: input,
        },
      },
    };
    console.log('chat message : ', dataLocal);
    sendMessage(dataLocal);
  };

  const onShare = async () => {
    try {
      const result = await Share.share({
        message: `Hello, User ! Lets surf with Fiknet App ${'\n'}bit.ly/fiknetApp`,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  return (
    <View>
      <Headers
        type="triple"
        title={data.category}
        iconName="share-social"
        onPressBack={() => navigation.goBack()}
        onPressRight={() => {
          onShare();
        }}
      />

      <ScrollView
        showsVerticalScrollIndicator={false}
        style={styles.subContainer}>
        <View style={styles.container}>
          <Gap height={15} />
          <Text style={styles.title}>{data.tittle}</Text>
          <Gap height={10} />
          <View style={styles.date}>
            <Icon style={styles.iconStyle} name="time-outline" />
            <Text style={styles.dateText}>
              {moment(data.createdAt).fromNow()}
            </Text>
          </View>
          <Gap height={10} />
          <Image
            source={{
              uri: `${base_img}/${data.image}`,
            }}
            style={styles.image}
          />
          <Gap height={15} />
          <View>
            <Text style={styles.desc}>{data.desc}</Text>
          </View>
          <Gap height={10} />
          <View style={styles.comment}>
            <Text style={styles.commentText}>Komentar</Text>
            <View style={styles.boundary} />
            <Text style={styles.commentText}>{messages.length}</Text>
          </View>
          <Gap height={10} />
          {messages.map((item, key) => {
            return (
              <>
                <Comment
                  key={item.id}
                  userName={item.name}
                  date={item.allChat.chatText.chatTime}
                  photos={
                    item.image
                      ? item.image
                      : 'https://www.cornwallbusinessawards.co.uk/wp-content/uploads/2017/11/dummy450x450.jpg'
                  }
                  content={item.allChat.chatText.chatContent}
                />
                <Gap height={22} />
              </>
            );
          })}
          <View style={styles.searchSection}>
            <TextInput
              style={styles.input}
              placeholder="Tulis Komentar"
              value={input}
              onChangeText={(value) => setInput(value)}
            />
            <TouchableOpacity onPress={handleSubmit}>
              <Icon style={styles.searchIcon} name="paper-plane" />
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default PengumumanDetail;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 15,
  },
  subContainer: {
    marginBottom: '20%',
  },
  title: {
    fontSize: 28,
    fontWeight: 'bold',
    color: colors.text.primary,
  },
  date: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconStyle: {
    fontSize: 17,
    color: colors.text.subTitle,
  },
  dateText: {
    paddingLeft: '2%',
    fontSize: 14,
    color: colors.text.subTitle,
  },
  image: {
    height: 304,
    borderRadius: 12,
    resizeMode: 'cover',
  },
  desc: {
    fontSize: 14,
    fontWeight: '200',
    color: colors.text.subTitle1,
    opacity: 0.7,
    textAlign: 'justify',
  },
  comment: {
    flexDirection: 'row',
  },
  commentText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.text.title,
  },
  boundary: {
    marginHorizontal: 15,
    backgroundColor: colors.primaryBlack,
    height: 19,
    width: 3,
    alignSelf: 'center',
  },

  inputComment: {
    backgroundColor: colors.primaryWhite,
    borderRadius: 8,
    borderColor: colors.primaryBlack,
    borderWidth: 1,
    paddingHorizontal: 12,
    paddingVertical: 10,
    marginVertical: 20,
  },
  searchSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.primaryWhite,
    borderRadius: 8,
    borderColor: colors.primaryBlack,
    borderWidth: 1,
    paddingHorizontal: 12,
    marginVertical: 20,
  },
  searchIcon: {
    fontSize: 22,
    color: colors.primary,
  },
  input: {
    flex: 1,
    paddingLeft: 0,
    backgroundColor: '#fff',
    color: '#424242',
  },
});
