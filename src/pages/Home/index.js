import axios from 'axios';
import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {colors, getData, base_url} from '../../utils';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Gap, StatusBars} from '../../components/atoms';
import {Banner, Category} from '../../components/moleculs';

const Home = ({navigation}) => {
  const [event, setEvent] = useState([]);
  useEffect(() => {
    renderData();
    const unsubscribe = navigation.addListener('focus', () => {
      renderData();
    });
    return unsubscribe;
  }, [navigation]);

  const renderData = () => {
    getData('login_log').then((res) => {
      if (res) {
        getEvent(res.token);
      }
    });
  };
  const getEvent = (token) => {
    axios
      .get(`${base_url}/event/posts`, {
        headers: {
          Authorization: `bearer ${token}`,
        },
      })
      .then((response) => {
        setEvent(response.data.data);
        console.log('data berhasil diretrive ', response.data.data);
      })
      .catch((error) => {
        console.log('error : ', error.message);
      });
  };

  return (
    <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
      <StatusBars />
      <Image
        source={require('../../assets/Geradasi-header.png')}
        style={styles.bgHeaders}
      />
      <View style={styles.headers}>
        <Text style={styles.header_text}>FIKNET</Text>
        <View style={styles.bundle_Notifications}>
          <Image
            source={require('../../assets/notifications.png')}
            style={styles.imageNotifications}
          />
          <View style={styles.notifications} />
        </View>
      </View>
      <Gap height={15} />
      <View style={styles.content}>
        <TouchableOpacity
          style={styles.search}
          onPress={() =>
            navigation.navigate('Searching', {url: 'event/posts'})
          }>
          <Icon name="search" style={styles.iconStyle} />
          <Text style={styles.textInput}>Cari</Text>
        </TouchableOpacity>
        <Gap height={35} />
        <View style={styles.category}>
          <Category
            photos={require('../../assets/logo-news.png')}
            title="Informasi Umum"
            onPress={() =>
              navigation.navigate('Pengumuman', {title: 'Informasi Umum'})
            }
          />
          <Category
            photos={require('../../assets/logo-berita.png')}
            title="Berita Kampus"
            onPress={() =>
              navigation.navigate('Pengumuman', {title: 'Berita Kampus'})
            }
          />
          <Category
            photos={require('../../assets/logo-promotions.png')}
            title="Pengumuman"
            onPress={() =>
              navigation.navigate('Pengumuman', {title: 'Pengumuman'})
            }
          />
          <Category
            photos={require('../../assets/logo-menu.png')}
            title="Lainnya"
            onPress={() => navigation.navigate('Lainnya')}
          />
        </View>
        <Gap height={20} />
        <Text style={styles.title}>Berita Terbaru</Text>
        <Gap height={10} />

        {event.reverse().map((item) => {
          return (
            <>
              <Banner
                key={item.id}
                photo={item.image}
                title={item.tittle}
                time={item.createdAt}
                category={item.category}
                onPress={() => navigation.navigate('Pengumuman Detail', item)}
              />
              <Gap height={20} />
            </>
          );
        })}
      </View>
    </ScrollView>
  );
};

export default Home;

const styles = StyleSheet.create({
  mediaImage: {
    height: 105,
    resizeMode: 'cover',
  },
  container: {
    flex: 1,
    backgroundColor: colors.primaryWhite,
  },
  headers: {
    paddingVertical: 20,
    paddingHorizontal: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    zIndex: 1,
    backgroundColor: colors.primary,
    paddingTop: '10%',
  },
  bgHeaders: {
    position: 'absolute',
    zIndex: 0,
    alignSelf: 'center',
    marginTop: '15%',
    width: '100%',
    resizeMode: 'stretch',
  },
  header_text: {
    color: colors.text.secondary,
    fontSize: 24,
    fontWeight: 'bold',
  },
  notifications: {
    position: 'absolute',
    alignSelf: 'flex-end',
    width: 12,
    height: 12,
    borderRadius: 7,
    marginTop: '10%',
    backgroundColor: colors.notifications,
    borderWidth: 2,
    borderColor: colors.primaryWhite,
  },
  content: {
    marginHorizontal: 15,
  },
  search: {
    zIndex: 1,
    height: 40,
    borderRadius: 20,
    backgroundColor: colors.primaryWhite,
    flexDirection: 'row',
    elevation: 1,
    alignItems: 'center',
  },
  imageNotifications: {
    width: 30,
    height: 35,
  },
  iconStyle: {
    marginLeft: 20,
    fontSize: 20,
    color: colors.text.subTitle,
  },
  textInput: {
    marginLeft: 15,
    fontSize: 17,
    color: colors.text.subTitle,
  },
  category: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    zIndex: 1,
  },
  title: {
    color: colors.text.title,
    fontSize: 14,
    fontWeight: 'bold',
  },
});
