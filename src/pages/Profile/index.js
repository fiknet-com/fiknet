import axios from 'axios';
import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, ScrollView, Image} from 'react-native';
import {base_url, colors, getData} from '../../utils';
import {Announcement, Headers, Timeline} from '../../components/moleculs';
import {Buttons, Gap, StatusBars, TextInputs} from '../../components/atoms';
import AsyncStorage from '@react-native-community/async-storage';

const Profile = ({navigation}) => {
  const [profile, setProfile] = useState([]);
  const [photo, setPhoto] = useState(
    'https://i.ibb.co/L82vc7B/profile-base.png',
  );
  useEffect(() => {
    renderData();
    const unsubscribe = navigation.addListener('focus', () => {
      renderData();
    });
    return unsubscribe;
  }, [navigation]);

  const renderData = () => {
    getData('login_log').then((res) => {
      if (res) {
        getProfile(res.token, res.id);
      }
    });
  };
  const getProfile = (token, id) => {
    axios
      .get(`${base_url}/auth/user/${id}`, {
        headers: {
          Authorization: `bearer ${token}`,
        },
      })
      .then((response) => {
        setProfile(response.data.data);
        if (response.data.data.image != undefined) {
          setPhoto(response.data.data.image);
        }
        console.log('data profile image diretrive ', response.data.data);
      })
      .catch((error) => {
        console.log('error : ', error.message);
      });
  };

  const clearSession = async () => {
    await AsyncStorage.clear();
    navigation.replace('Login');
  };
  return (
    <ScrollView style={styles.container}>
      <StatusBars />
      <Headers
        type="edit"
        title="Profil"
        iconName="pencil"
        onPressRight={() => navigation.navigate('Edit Profile', profile)}
      />
      <View style={styles.topSection}>
        <Image source={{uri: photo}} style={styles.image} />
        <Image
          source={require('../../assets/profile_bg.png')}
          style={styles.imageBg}
        />
      </View>
      <View style={styles.bundle}>
        <Gap height={30} />
        <TextInputs edit={false} value={profile.name} title="Nama" />
        <Gap height={20} />
        <TextInputs edit={false} value={profile.nim} title="NIM" />
        <Gap height={20} />
        <TextInputs edit={false} value={profile.email} title="Email" />
        {/* <Gap height={20} />
        <TextInputs
          edit={false}
          value={profile.password}
          title="Kata Sandi"
          secure
        /> */}
        <Gap height={50} />
        <Buttons title="Keluar" onPress={clearSession} />
      </View>
    </ScrollView>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primaryWhite,
  },
  topSection: {
    height: 122,
    borderBottomLeftRadius: 25,
    borderBottomRightRadius: 25,
    backgroundColor: colors.primary,
  },
  image: {
    width: 110,
    height: 110,
    borderRadius: 55,
    resizeMode: 'cover',
    alignSelf: 'center',
    zIndex: 1,
  },
  imageBg: {
    position: 'absolute',
    zIndex: 0,
    alignSelf: 'center',
    width: '100%',
    resizeMode: 'stretch',
  },
  bundle: {
    marginHorizontal: 13,
    flex: 1,
    marginTop: '10%',
  },
});
